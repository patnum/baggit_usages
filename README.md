# bagit_et_ses_usages

# Lisez moi

Cete page rassemble différents implémentations ou usages de bagit qui nous semblent intéressants à partager.

Les projets suivants sont présentés:

- script bagit à la BCUL
- script bagit à la BCUF

## Bagit à la BCUL
A compléter... 

## Bagit à la BCUF

### Description du projet
Ce script a été élaboré initialement pour répondre à plusieurs besoins:
 - générer des paquets temporaires en attente d'archivage,
 - être compatible comme entrée à Archivematica,
 - embarquer certaines métadonnées à destination de AtoM,
 - conserver la structure hierarchique des dossiers dans AtoM ([voir note plus bas](https://framagit.org/patnum/baggit_usages/-/blob/main/README.md#dip-vers-atom)).  

Une autre demande ultérieure est apparue: produire un interface graphique au script devant fonctionner sous macOs X et windows. 

Les deux sous projets sont présentés ci-dessous. 

### Script python fd_bagger
Le script et sa documentation est accessible ici https://github.com/BCUF/fd_bagger.

### Contexte d'usage de bagit à la BCUF
Les bags sont utilisés par l'archiviste numérique pour générer des paquets SIP à destination de archivematica. 
Chaque paquet est considéré comme un versement à l'archive. Il est nommé suivant le nom du fonds suivi d'une incrémentation du nombre de versements relatifs à ce fonds. 

Le contenu de ces paquets de versement correspond parfois à un support informatique. Par exemple, si l'on souhaite archiver un fonds contenant: 
- un disque dur
- 24 clefs USB
- 200 CD-R
225 paquets de versemment seront produits.

| media | nommage du paquet |
| ------ | ------ |
|   disque dur     |   ARCHNUMFR_XXXX-0001     |
|     clef USB 1   |     ARCHNUMFR_XXXX-0002   |
|     clef USB 2   |     ARCHNUMFR_XXXX-0003   |
|     clef ....   |     ARCHNUMFR_XXXX-00..   |
|     clef USB 24   |     ARCHNUMFR_XXXX-0025   |
|     CD-R 1       |  ARCHNUMFR_XXXX-0026   |
|     CD-R ...       |  ARCHNUMFR_XXXX-0...   |
|     CD-R 200   |     ARCHNUMFR_XXXX-0225   |



Cependant dans notre contexte d'utilisation avec Archivematica, il n'est pas souhaitable de fournir en entrée des paquets de versement sous forme de bagg trop volumineux ou contenant trop de fichier. En effet, plus le paquet est volumineux, plus le temps de traitement est long. De plus lorsqu'une erreur apparait, c'est l'entièreté du paquet qui est rejeté. 
Dans le cas d'un disque contenant plus de 150 Go, son contenu sera découpé en plusieurs paquets de versement donc en plusieurs bags. 

#### DIP vers AtoM
Jusqu'à la version 2.6.2 d'AtoM, la hierarchie originale des dossiers du système de fichier d'un support informatique est certes recueillie et enregistrée dans le METS qui accompagne l'AIP. Cependant la structure n'est pas représentée dans AtoM une fois l'archive téléversée dans AtoM. Pour faciliter le travail des archivistes et conservateurs, AtoM a été forké afin de permettre qu'un versement soit automatiquement hierarchisé selon l'organisation originale des dossiers du support informatique dans AtoM. 

### interface graphique du script fd_bagger
Ce script est encore en béta donc n'est pas encore accessible sur le https://github.com/BCUF. 

### License
GNU General Public License v3.0
### Project status
Projet en pause pour l'instant car répond à nos besoins.
